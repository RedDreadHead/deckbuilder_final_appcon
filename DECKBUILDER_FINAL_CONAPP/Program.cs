﻿using System;
using System.Security.Cryptography;
using DECKBUILDER_lib.Objects;
using DECKBUILDER_lib.Tables;
using MtgApiManager.Lib.Service;
/*
karta jako jediná nemá generované ID
set jako jediný nemá int ID (má string set_code)

*/


namespace DECKBUILDER_FINAL_CONAPP{
    internal class Program{
        public static void Main(string[] args) {
              //test_selecty();
              var dt = new DecksTable();
              var ds = dt.Select_best();                            
              foreach (var d in ds) { Console.WriteLine(d); }
            //test_crud();
            
            //new ChangesTable().Apply_change(new Changes("A", 461177, 4, 2));//11.2
            //new Card_deckTable().Delete_card_from_deck(new Card_deck(1,1,461177));//6.2
            //test_rate(0,3,2);//5.1, 5.2 
            //new PlayersTable().Log_in("gertruda", "heslo"); //11.1
            }

        public static void test_crud() {
          
            new PlayersTable().Insert(new Players("klotilda"));//1.1
            new RatingsTable().Update(new Ratings(3, 1, 2)); //5.2
            new CommentsTable().Delete(new Comments());//4.3
            
            var dt = new DecksTable();
            var ds = dt.Select();                            //3.4
            foreach (var d in ds) { Console.WriteLine(d); }
         }

        public static void test_selecty() { 
            //foreach (var o in new CardsTable().Select()) { Console.WriteLine(o); }//2.4 CRUD
            test_get_cards_by_name("ajani"); //2.4.2
            test_get_cards_by_deck(1); //2.4.3
            test_get_avg_cmc(3); //2.4.4
            test_comments_in_deck(1); //4.4 
            test_color_ratio(3); //2.4.4
            test_most_voting_player_cards();//11.4
            

        }
        public static void test_add_comm() {
            var ct = new CommentsTable();
            var c = new Comments("ahoj mami",6,6);
            ct.Add_comment(c);
        }
        public static void test_deck_insert() {
            var dt = new DecksTable();
            var d = new Decks();
            d.name = "balicekkaret";
            d.description = "tohle je supr cupr deck";
            d.player_id = 6;
            dt.Create_deck(d);
        }
       
        public static void test_comments_in_deck(int deck_id) {
            Console.WriteLine("Selectuji komentare k decku "+deck_id);
            var ct = new CommentsTable();
            var cs = ct.Select_by_deck_id(deck_id);
            foreach (var c in cs) {
                Console.WriteLine(c);
            }
        }
        public static void test_get_avg_cmc(int deck_id) {
            var ct = new CardsTable();
            var avg = ct.Select_avg_cmc(deck_id);
            Console.WriteLine("Prumerna cena karty v decku "+deck_id+" je: "+avg+" cmc");
        }
        public static void test_get_cards_by_name(string param) {
            var ct = new CardsTable();
            var cs = ct.Select_by_name(param);
            foreach (var c in cs) {
                Console.WriteLine(c);
            }
        }
        public static void test_get_cards_by_deck(int deck_id) {
            var ct = new CardsTable();
            var cs = ct.Select_by_deck_id(deck_id);
            foreach (var c in cs) {
                Console.WriteLine(c);
            }
        }
        public static void test_most_voting_player_cards() {
            var ct = new CardsTable();
            var cs = ct.Select_cards_most_rating_player();
            foreach (var c in cs) {
                Console.WriteLine(c);
            }
            
        }
        public static void test_rate(int stars, int player_id, int deck_id) {
            var rt = new RatingsTable();
            var r = new Ratings(stars,player_id,deck_id);
            int i = rt.Rate(r);
            switch (i) {
                case 2:
                    Console.WriteLine("Player "+r.player_id+" rated deck "+r.deck_id+" with "+r.stars+" stars");
                    break;
                case -1:
                    Console.WriteLine(r.stars+" is not between 0 and 5");
                    break;
            }
        }
        public static void test_color_ratio(int deck_id) {
            
            var d = new DecksTable().Select(deck_id);

            var ratio = new CardsTable().Select_color_ratio(d);
            Console.WriteLine("\nRATIO\tbarva \tkarty \tprocenta \t\t v balíčku "+d.name+
                              "\n\t------------------------------------------------");
            foreach  (var  c  in ratio) {
                Console.WriteLine("\t"+c["color"]+"\t"+c["cards"]+"\t"+c["percent"]);
            }
        }
        /// OTHER STUFF/////////////////////////////////////////////////////////////////////////////
       
    
        private static void generate_inserts() {
            int[] MIDs ={413591,433008,420645,389477,383006};
                foreach(int i in MIDs) {
                generate_insert_by_id(i);
            }
        }
    
        private static void generate_insert_by_id(int MID) {
    
            CardService service = new CardService();
            var result = service.Find(MID);
            var res = result.Value;
                    
            var power = res.Power ?? "null";
            var toughness = res.Toughness??"null";
            var cmc = res.Cmc ?? 0;
            var name = res.Name.Replace("'","''");
            var rarity = 3;
            var type = "";
            
            if (power != "null") { type = "creature"; }
            else { type = res.Types[0].ToLower();}
                    
            if (res.Rarity.ToLower() == "common") { rarity = 0; }
            else if (res.Rarity.ToLower() == "uncommon") { rarity = 1; }
            else if (res.Rarity.ToLower() == "rare") { rarity = 2; }
                    
            Console.WriteLine(
            $"insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values ({res.MultiverseId}, '{name}', {cmc},'{res.Set}','{res.Colors[0].ToLower()}','{type}',{power},{toughness},'{res.ImageUrl}','{res.Artist}',{rarity})");
        }
        

    }
}