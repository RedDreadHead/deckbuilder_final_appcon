using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class ChangesTable{
        private const string SQL_INSERT = "INSERT INTO changes (change_type, card_id, comment_id, count) values (@change_type, @card_id, @comment_id, @count)";
        private const string SQL_SELECT = "SELECT change_type, card_id, comment_id, count FROM changes";
        private const string SQL_UPDATE = "UPDATE changes SET change_type=@change_type, count=@count where comment_id = @comment_id and card_id = @card_id";
        private const string SQL_DELETE = "DELETE from CHANGES where  comment_id = @comment_id and card_id = @card_id";
        private Database.Database db;

        public ChangesTable() {
            db = new Database.Database();
        }

/// NOT CRUD /////////////////////////////////////////////////////////////
        public int Apply_change(Changes change) {//11.1
            Console.WriteLine("Aplikuji změnu: "+change);
            using (db.GetConnection) {
                db.Connect();
                using (var cmd = new SqlCommand("apply_change")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    cmd.Parameters.Add("@ret", SqlDbType.Int);
                    cmd.Parameters["@ret"].Direction = ParameterDirection.Output;
                    Prepare_command(cmd,change);
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tKladné číslo = úspěch při aplikaci změny:\t "+cmd.Parameters["@ret"].Value);
                    return (int) cmd.Parameters["@ret"].Value;
                }                
            }          
        }

/// CRUD /////////////////////////////////////////////////////////////
        public int Insert(Changes change) {//9.1
            Console.WriteLine("Vkladam: " + change);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);

                Prepare_command(command, change);

                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }


        public int Update(Changes change) {//9.3
            Console.WriteLine("Updatuji: " + change);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);

                Prepare_command(command, change);

                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }

        public int Delete(Changes change) {//9.2
            Console.WriteLine("Mazu zaznam: " + change);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);

                Prepare_command(command, change);

                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Changes> Select() {//9.4
            Console.WriteLine("Selectuji vsechny zmeny");
            using (db.GetConnection) {
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using (var reader = db.Select(command)) {
                    Collection<Changes> changes = Read(reader);
                    return changes;
                }
            }

        }
/// OTHERS /////////////////////////////////////////////////////////////
        private static Collection<Changes> Read(SqlDataReader reader) {
            var changes = new Collection<Changes>();
            while (reader.Read()) {
                var ch = new Changes(
                    reader.GetString(0),
                    reader.GetInt32(1),
                    reader.GetInt32(2),
                    reader.GetInt32(3)
                );
                changes.Add(ch);
            }

            return changes;
        }
        
        private void Prepare_command(SqlCommand c, Changes ch) {
            c.Parameters.Add("@change_type",SqlDbType.VarChar, 1).Value =  ch.change_type;
            c.Parameters.Add("@card_id", SqlDbType.Int).Value=ch.card_id;
            c.Parameters.Add("@comment_id",SqlDbType.Int).Value= ch.comment_id;
            c.Parameters.Add("@count", SqlDbType.Int).Value=ch.count;
        }
    }
}