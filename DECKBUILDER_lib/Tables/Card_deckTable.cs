using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Objects;
using System.Configuration;

namespace DECKBUILDER_lib.Tables{
    public class Card_deckTable{
        
private const string SQL_INSERT = "INSERT INTO Card_deck (count, card_id, deck_id) values (@count, @card_id, @deck_id)";
private const string SQL_SELECT = "SELECT count, deck_id, card_id FROM card_deck";
private const string SQL_UPDATE = "UPDATE card_deck SET count = @count where card_id=@card_id and deck_id=@deck_id";
private const string SQL_DELETE = "DELETE FROM card_deck where card_id=@card_id and deck_id=@deck_id";
      
        private Database.Database db;

        public Card_deckTable() {
            db = new Database.Database();
        }
       

        /// NOT CRUD ///////////////////////////////////////////////////////////// 
        public int Delete_card_from_deck(Card_deck cd) {
            using (db.GetConnection) {
                db.Connect();
                using (var cmd = new SqlCommand("delete_card_from_deck")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    cmd.Parameters.AddWithValue("@card_id",cd.card_id);
                    cmd.Parameters.AddWithValue("@deck_id", cd.deck_id);
                    cmd.Parameters.AddWithValue("@count", cd.count);
                    cmd.Parameters.Add("@ret", SqlDbType.Int);
                    cmd.Parameters["@ret"].Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tKladné číslo = úspěch při mazání karty z decku:\t "+
                                      cmd.Parameters["@ret"].Value);
                    return (int) cmd.Parameters["@ret"].Value;

                }
            }
        }

        public int Add_card_to_deck(Card_deck cd) { //6.1, 6.3
            using (db.GetConnection) {
                Console.WriteLine("Vkládám kartu "+cd.card_id+" do decku "+cd.deck_id+"\t"+cd.count+"x");
                db.Connect();
                using (var cmd = new SqlCommand("add_card_to_deck")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    cmd.Parameters.AddWithValue("@card_id", cd.card_id);
                    cmd.Parameters.AddWithValue("@deck_id", cd.deck_id);
                    cmd.Parameters.AddWithValue("@count", cd.count);
                    cmd.Parameters.Add("@ret", SqlDbType.Int);
                    cmd.Parameters["@ret"].Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tKladné číslo = úspěch při přidávání karty do decku:\t " +
                                      cmd.Parameters["@ret"].Value.ToString());
                    return (int) cmd.Parameters["@ret"].Value;
                }
            }
        }
        /// CRUD /////////////////////////////////////////////////////////////  
        public int Insert(Card_deck cd) {
            Console.WriteLine("Vkladam: "+cd);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, cd);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Card_deck cd) {
            Console.WriteLine("Updatuji: "+cd);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, cd);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Card_deck cd) {
            Console.WriteLine("Mazu zaznam:" + cd);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command, cd);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Card_deck> Select() {
            Console.WriteLine("Selectuji vsechny vazby card_deck");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Card_deck> cds = Read(reader);
                    return cds;
                }
            }
        }
        
        /// OTHERS /////////////////////////////////////////////////////////////  
        
        private static Collection<Card_deck> Read(SqlDataReader reader) {
            var cds = new Collection<Card_deck>();
            while (reader.Read()) {
                var cd = new Card_deck(
                    reader.GetInt32(0),
                    reader.GetInt32(1),
                    reader.GetInt32(2));
                cds.Add(cd);              
            }
            return cds;
        }

        private static void Prepare_command(SqlCommand c, Card_deck cd) {
            //com.param.add("@cosi",SQLTYPE).Value=hodnota
            c.Parameters.Add("@count",SqlDbType.Int).Value=cd.count;

            c.Parameters.Add("@card_id",SqlDbType.Int).Value=cd.card_id;

            c.Parameters.Add("@deck_id",SqlDbType.Int).Value=cd.deck_id;
        }

    }
}