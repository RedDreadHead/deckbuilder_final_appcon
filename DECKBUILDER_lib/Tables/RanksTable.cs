using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class RanksTable{ 
        private Database.Database db;
        
        public RanksTable() {
            db = new Database.Database();
        }
        public Collection<Ranks> Select() {//10
            Console.WriteLine("Selectuji vsechny ranky");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand("select rank_name, stars from ranks");
                using(var reader = db.Select(command)){
                    Collection<Ranks> ranks = Read(reader);
                    return ranks;
                }
            }
        }

        private static Collection<Ranks> Read(SqlDataReader reader) {
            var ranks = new Collection<Ranks>();
            while (reader.Read()) {
                var r = new Ranks(reader.GetString(0),reader.GetInt32(1));
                ranks.Add(r);
            }
            return ranks;
        }
        
        
        private static void Prepare_command(SqlCommand c, Ranks r) {
            c.Parameters.Add("@stars", SqlDbType.Int).Value = r.stars;
            
            c.Parameters.Add("@rank_name", SqlDbType.VarChar, Ranks.LEN_ATTR_rank_name).Value = r.rank_name;            
           
        }
    }
}