using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Interfaces;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class CardsTable:ITable<Cards>{
        //2.CRUD
        private const string SQL_INSERT = "INSERT INTO Cards (card_id, NAME, TYPE, COLOR, ARTIST, IMAGE, CMC, RARITY, POWER, TOUGHNESS, SET_CODE) VALUES (@card_id, @name, @type, @color, @artist, @image, @cmc, @rarity, @power, @toughness, @set_code)";
        private const string SQL_UPDATE = "UPDATE cards SET name=@name, type=@type, color=@color, artist=@artist, image=@image, cmc=@cmc, rarity=@rarity, power = @power, toughness=@toughness, set_code=@set_code where card_id = @card_id";
        private const string SQL_DELETE = "UPDATE cards SET deleted_at = sysdatetime() where card_id = @card_id";
        private const string SQL_SELECT = "SELECT card_id, name, type, color, artist, image, cmc, rarity, power, toughness, set_code,deleted_at FROM cards order by set_code";        
        // 2.4.1 
        private const string SQL_SELECT_TOP_CARDS_IN_DECK = "select * from cards where  card_id in(SELECT TOP 5 cd.card_id FROM card_deck cd JOIN cards c ON c.card_id = cd.card_id AND cd.deck_id = @deck_id and c.type not like 'land' GROUP BY cd.CARD_ID ORDER BY SUM(cd.count) desc)";
        private const string SQL_SELECT_TOP_CARDS_IN_SET = "select * from cards where  card_id in(SELECT TOP 5 cd.card_id FROM card_deck cd JOIN cards c ON c.card_id = cd.card_id AND c.set_code = @set_code and c.type not like 'land' GROUP BY cd.CARD_ID ORDER BY SUM(cd.count) desc)";
        //2.4.4
        private const string SQL_SELECT_COLOR_RATIO = "SELECT c.color, SUM(count) AS Pocet_karet, (SUM(count) * 100.0)/ (SELECT SUM(count) FROM card_deck WHERE deck_id = @deck_id) AS Procenta FROM cards c JOIN card_deck cd ON c.card_id=cd.card_id AND cd.deck_id=@deck_id GROUP BY c.color";
        private const string SQL_SELECT_TYPE_RATIO = "SELECT c.type, SUM(count) AS Pocet_karet, (SUM(count) * 100.0)/ (SELECT SUM(count) FROM card_deck WHERE deck_id = @deck_id) AS Procenta FROM cards c JOIN card_deck cd ON c.card_id=cd.card_id AND cd.deck_id=@deck_id GROUP BY c.type";
        //11.5
        private const string SQL_SELECT_MOST_VOTING = " select * from cards where card_id in( select TOP 5 c.card_id from card_deck cd join cards c on c.card_id = cd.card_id and deck_id in( select d.deck_id from decks d where deck_id = cd.deck_id and d.player_id in( select TOP 1 p.player_id from players p join ratings r on r.player_id = p.player_id group by p.player_id order by count(r.player_id) desc ) ) group by c.card_id order by sum(cd.count) DESC)";

        private const string SQL_SELECT_COUNT_CARDS =
            "select cd.count,c.name from cards c join card_deck cd on c.card_id = cd.card_id and cd.deck_id = @deck_id order by c.type";//TODO
        private Database.Database db;
        public CardsTable() {
            db = new Database.Database();
        }
        /// NOT CRUD /////////////////////////////////////////////////////////////          
        public Collection<Cards> Select_cards_most_rating_player() {
            Console.WriteLine("Selectuji nejpouzivanejsi karty v decich nejcasteji hodnoticiho hrace");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_MOST_VOTING);
                using(var reader = db.Select(command)){
                    Collection<Cards> cds = Read(reader);
                    return cds;
                }
            }
        }
        
    public Collection<Cards> Select_top_5_in_deck(Decks deck) { //11.4, 2.4.1
            Console.WriteLine("Selectuji TOP 5 z decku "+deck);
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_TOP_CARDS_IN_DECK);
                
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck.deck_id;
                
                using(var reader = db.Select(command)){
                    Collection<Cards> cds = Read(reader);
                    return cds;
                }
            }
        }
        
        public List<Dictionary<string, string>> Select_color_ratio(Decks deck) { //2.4.4
            //Console.WriteLine("Selectuji pomer barev v decku "+deck);
            using (db.GetConnection) {
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_COLOR_RATIO);
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck.deck_id;
                using (var reader = db.Select(command)) {
                    List<Dictionary<string, string>> ratio = Read_ratio(reader);
                    return ratio;
                }
            }
        }
        
        public List<Dictionary<string, string>> Select_type_ratio(Decks deck) { //2.4.4
            Console.WriteLine("Selectuji pomer typů v decku "+deck);
            using (db.GetConnection) {
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_TYPE_RATIO);
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck.deck_id;
                using (var reader = db.Select(command)) {
                    List<Dictionary<string, string>> ratio = Read_ratio(reader);
                    return ratio;
                }
            }
        }

        public  string Select_avg_cmc(int deck_id) { //2.4.4
            using (db.GetConnection) {
                db.Connect();
                SqlCommand command = db.CreateCommand("select avg(cmc) from cards where card_id in(select card_id from card_deck where deck_id = @deck_id)");
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck_id;
                //Console.WriteLine(command.CommandText);
                using (var reader = db.Select(command)) {
                    string avg = Read_avg(reader);
                    return avg;
                }
            }
            
        }
        
        public Collection<Cards> Select_top_5_in_set(Sets set) {//11.4, 2.4.1
            Console.WriteLine("Selectuji TOP 5 ze setu "+set);
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_TOP_CARDS_IN_SET);
                
                command.Parameters.Add(new SqlParameter("@set_code", SqlDbType.VarChar, Sets.LEN_ATTR_set_code));
                command.Parameters["@set_code"].Value = set.set_code;
                
                using(var reader = db.Select(command)){
                    Collection<Cards> cds = Read(reader);
                    return cds;
                }
            }
        }

        public Collection<Cards> Select_by_name(string name_str) {//2.4.2
            Console.WriteLine("Hledam kartu \""+name_str+"\"");
            using (db.GetConnection) {
                db.Connect();
                SqlCommand command = db.CreateCommand("select * from cards c where c.name like '%'+@name_str+'%'");
                
                command.Parameters.Add(new SqlParameter("@name_str", SqlDbType.VarChar, Cards.LEN_ATTR_name));
                command.Parameters["@name_str"].Value = name_str;
                
                //Console.WriteLine(command.CommandText);
                
                using(var reader = db.Select(command)){
                    Collection<Cards> cds = Read(reader);
                    return cds;
                }
            }
        }

        public Collection<Cards> Select_by_deck_id(int deck_id) {
            Console.WriteLine("Select karet z decku \""+deck_id+"\"");
            using (db.GetConnection) {
                db.Connect();
                SqlCommand command = db.CreateCommand("select * from cards c where c.card_id in(select card_id from card_deck where deck_id = @deck_id)");
                
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck_id;
                
                //Console.WriteLine(command.CommandText);
                
                using(var reader = db.Select(command)){
                    Collection<Cards> cds = Read(reader);
                    return cds;
                }
            }
        }

        /// CRUD /////////////////////////////////////////////////////////////      
        public int Insert(Cards card) {//2.1
            Console.WriteLine("Vkladam: "+card);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, card);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Cards card) {//2.2
            Console.WriteLine("Updatuji: "+card);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, card);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        public int Delete(Cards card) {//2.3
            Console.WriteLine("Deaktivuji: " + card);
            using(db.GetConnection){
                
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,card);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Cards> Select() {//2.4
            Console.WriteLine("Selectuji vsechny karty:");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Cards>cards = Read(reader);
                    return cards;
                }
            }
        }


        /// OTHERS /////////////////////////////////////////////////////////////
        private static string Read_avg(SqlDataReader reader) {
            string result = "No result";
            while (reader.Read()) {
                result = reader[0].ToString();
                //Console.WriteLine(result);
            }

            return result;
        }
        private static List<Dictionary<string,string>> Read_ratio(SqlDataReader reader) {
            var results = new List<Dictionary<string, string>>();
            while (reader.Read()) {
                var result=new Dictionary<string, string>();
                result.Add("color", reader[0].ToString());
                result.Add("cards", reader[1].ToString());
                result.Add("percent", reader[2].ToString());
                results.Add(result);
            }
            return results;
        }
        private static Collection<Cards> Read(SqlDataReader reader) {
            var cards = new Collection<Cards>();
            while (reader.Read()) {
                var c = new Cards();
                
                c.card_id = reader.GetInt32(0);
                c.name = reader.GetString(1);
                c.type = reader.GetString(2);
                c.color = reader.GetString(3);
                c.artist = reader.GetString(4);
                c.image = reader.GetString(5);
                c.cmc = reader.GetInt32(6);
                c.rarity = reader.GetInt32(7);
                c.power = Get_num_from_db(reader[8]);//
                c.toughness = Get_num_from_db(reader[9]);//
                c.set_code = reader.GetString(10);
                c.deleted_at = Get_date_from_db(reader[11]);
                
                cards.Add(c);              
            }
            return cards;
        }
        
        private static DateTime? Get_date_from_db(object value) {
            return value == DBNull.Value ? (DateTime?) null : Convert.ToDateTime(value); 
        }
        private static int? Get_num_from_db(object value) {
            return value == DBNull.Value ? (int?) null : Convert.ToInt32(value);
        }
        
        private void Prepare_command(SqlCommand com, Cards card) {
            
            com.Parameters.Add("@card_id",SqlDbType.Int).Value = card.card_id;
            com.Parameters.Add("@name",SqlDbType.VarChar,Cards.LEN_ATTR_name).Value = card.name;
            com.Parameters.Add("@type",SqlDbType.VarChar,Cards.LEN_ATTR_type).Value = card.type;
            com.Parameters.Add("@color",SqlDbType.VarChar,Cards.LEN_ATTR_color).Value = card.color;
            com.Parameters.Add("@image",SqlDbType.VarChar,Cards.LEN_ATTR_image).Value = card.image;
            com.Parameters.Add("@artist",SqlDbType.VarChar, Cards.LEN_ATTR_artist).Value = card.artist;
            com.Parameters.Add("@cmc", SqlDbType.Int).Value = card.cmc;
            com.Parameters.Add("@set_code",SqlDbType.VarChar,Cards.LEN_ATTR_set_code).Value = card.set_code;
            com.Parameters.Add("@rarity",SqlDbType.Int).Value = card.rarity;
            
            com.Parameters.Add(new SqlParameter("@power", SqlDbType.Int));
            if (card.power == null) { com.Parameters["@power"].Value = DBNull.Value;}
            else { com.Parameters["@power"].Value = card.power; }

            com.Parameters.Add(new SqlParameter("@toughness", SqlDbType.Int));
            if (card.toughness == null) { com.Parameters["@toughness"].Value = DBNull.Value;}
            else{com.Parameters["@toughness"].Value = card.toughness;}
            
            com.Parameters.Add(new SqlParameter("@deleted_at", SqlDbType.DateTime));
            if (card.deleted_at == null) { com.Parameters["@deleted_at"].Value = DBNull.Value; }
            else { com.Parameters["@deleted_at"].Value = card.deleted_at;}  
        }
        
    }
}