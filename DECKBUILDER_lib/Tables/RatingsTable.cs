using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DECKBUILDER_lib.Interfaces;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class RatingsTable:ITable<Ratings>{
        private const string SQL_INSERT =
            "INSERT INTO Ratings (stars, player_id, deck_id) values(@stars, @player_id, @deck_id)";

        private const string SQL_SELECT = "select stars, player_id, deck_id from Ratings";
        private const string SQL_UPDATE = "update ratings set stars=@stars where player_id=@player_id and deck_id=@deck_id";
        private const string SQL_DELETE = "delete from ratings where player_id=@player_id and deck_id = @deck_id";
        private Database.Database db;



        public RatingsTable() {
            db = new Database.Database();
        }

        /// create or update ///////////////////////////////////////////////////////////// 
        public int Rate(Ratings rating) {//5.1, 5.2
            using (db.GetConnection) {
                db.Connect();
                using (var cmd = new SqlCommand("p_rate")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    Prepare_command(cmd, rating);
                    var ret = cmd.ExecuteNonQuery();
                    return ret;
                }
                /*var command = db.CreateCommand("exec p_rate @stars, @player_id, @deck_id"); //
                Prepare_command(command, rating);
                var ret = db.ExecuteNonQuery(command); //-1 = stars error, 2 = ok
                return ret;*/
            }
        }
        //p_rate(@stars, @player_id, @deck_id)
            /*create or alter procedure p_rate @stars int, @player_id int, @deck_id int
                    as begin
                        declare @avg_rating numeric
                        if @stars not between 0 and 5
                            SELECT CAST(0 AS BIT)
                        else
                            begin
                                if(select count(stars) from ratings
                                where deck_id = @deck_id and player_id = @player_id) = 0
                                    insert into Ratings (stars, player_id, deck_id) values (@stars, @player_id, @deck_id)
                                else
                                    update Ratings set stars = @stars where player_id = @player_id and deck_id = @deck_id
                                select @avg_rating = avg(stars) from ratings where deck_id = @deck_id group by deck_id
                                update decks set stars =round(@avg_rating,0) where deck_id = @deck_id
                                
                                SELECT CAST(1 AS BIT)
                            end
                    end*/
        
        /// CRUD ///////////////////////////////////////////////////////////// 
        public int Insert(Ratings rating) {//5.1
            Console.WriteLine("Vkladam: "+rating);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, rating);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Ratings rating) {//5.2
            Console.WriteLine("Updatuji:"+rating);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, rating);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Ratings rating) {//5.3
            Console.WriteLine("Mazu zaznam:"+rating);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,rating);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Ratings> Select() {//5.4
            Console.WriteLine("Selectuji vsechna hodnoceni");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Ratings> ratings = Read(reader);
                    return ratings;
                }
            }
        }
        
        /// OTHERS ///////////////////////////////////////////////////////////// 
        private static Collection<Ratings> Read(SqlDataReader reader) {
            var ratings = new Collection<Ratings>();
            while (reader.Read()) {
                var r = new Ratings(
                    reader.GetInt32(0),
                    reader.GetInt32(1),
                    reader.GetInt32(2)
                    );
                
                
                ratings.Add(r);              
            }
            return ratings;
        }
        
        private static void Prepare_command(SqlCommand c, Ratings rating) {
            c.Parameters.Add("@stars", SqlDbType.Int).Value = rating.stars;   
            c.Parameters.Add("@player_id", SqlDbType.Int).Value = rating.player_id;   
            c.Parameters.Add("@deck_id", SqlDbType.Int).Value = rating.deck_id;          
            
            
        }
    }
}