using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class DecksTable{
        private const string SQL_INSERT = "INSERT INTO decks (name, description, max_cards_count, player_id) values (@name, @description, @max_cards_count, @player_id)";
        private const string SQL_UPDATE = "UPDATE decks set name = @name, description = @description, cards_count = @cards_count, stars = @stars, player_id=@player_id where deck_id = @deck_id";
       // private const string SQL_SELECT = "select deck_id, name, description, max_cards_count, cards_count, stars, player_id, deleted_at from decks";
        private const string SQL_SELECT_ID = "select deck_id, name, description, max_cards_count, cards_count, stars, player_id, deleted_at from decks where deck_id = @deck_id";
        private const string SQL_DELETE = "update decks set deleted_at = sysdatetime() where deck_id = @deck_id";

        private const string SQL_SELECT_BEST =
            "select top 3 d.deck_id, name, description, max_cards_count, cards_count, stars, player_id, deleted_at from decks d order by d.stars desc";
        private const string SQL_SELECT_WITH_ALL_COMMENTS = "select d.deck_id, d.name, d.description, d.max_cards_count,d.cards_count, d.stars, d.player_id, d.deleted_at, c.comment_id, c.text, c.added, c.player_id, c.deck_id from decks d left join comments c on d.deck_id = c.deck_id order by d.deck_id";
        private Database.Database db;
        
        public DecksTable() {
            db = new Database.Database();
        }

        /// NOT CRUD /////////////////////////////////////////////////////////////

        
        public int Create_deck(Decks deck) {
            Console.WriteLine("Vytvarim deck \""+deck.name+"\"");
            using (db.GetConnection) {
                db.Connect();
                using (var cmd = new SqlCommand("create_deck")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    cmd.Parameters.Add("@ret", SqlDbType.Int);
                    cmd.Parameters["@ret"].Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@name", deck.name);
                    cmd.Parameters.AddWithValue("@description", deck.description);
                    cmd.Parameters.AddWithValue("@player_id", deck.player_id);
                    
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tKladné číslo = úspěch při vytváření decku:\t ["+cmd.Parameters["@ret"].Value+"]");
                    return (int) cmd.Parameters["@ret"].Value;
                }                
            }    
        }
        
        public Decks Select(int deck_id) {
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_SELECT_ID);
                
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck_id;
                
                using (var reader = db.Select(command)) {
                    var decks = Read(reader);
                    Decks deck = null;

                    if (decks.Count == 1) {
                        deck = decks[0];
                        deck.comments = new CommentsTable().Select_by_deck_id(deck_id);
                    }
                    
                    
                    return deck;
                }
            }
        }
        
        public List<Decks> Get_comments() {//nope
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_SELECT_WITH_ALL_COMMENTS);
                using (var reader = db.Select(command)) {
                    var decks = new List<Decks>();
                    while (reader.Read()) {
                        var deck_id = reader.GetInt32(0);
                        var name = reader.GetString(1);
                        var description = Get_note_from_db(reader[2]);
                        var cards_count = reader.GetInt32(3);
                        var stars = Get_num_from_db(reader[4]);
                        var player_id = reader.GetInt32(5);
                        var comment_id = reader.GetInt32(6);
                        var text = reader.GetString(7);
                        var added = reader.GetDateTime(8);
                        
                        var deck = decks.FirstOrDefault(d=>d.deck_id==deck_id);
                        if (deck == null) {
                            deck = new Decks();
                            deck.description = description;
                            deck.name = name;
                            deck.cards_count = cards_count;
                            deck.stars = stars;

                            var comment = new Comments(text, player_id, deck_id);
                            comment.comment_id = comment_id;
                            comment.added_at = added;
                            deck.comments = new Collection<Comments> {comment};
                            decks.Add(deck);
                        }
                        else {
                            var comment = new Comments(text,player_id,deck_id){comment_id = comment_id, added_at = added};
                            if (!deck.comments.Contains(comment)) {
                                deck.comments.Add(comment);
                            }
                        }
                    }
                    return decks;
                }
            }
        }//nope
        /// CRUD ///////////////////////////////////////////////////////////// 
        public int Insert(Decks deck) {//3.1
            Console.WriteLine("Vkladam: "+deck);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, deck);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Decks deck) {//3.2
            Console.WriteLine("Updatuji:"+deck);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, deck);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Decks deck) {//3.3
            Console.WriteLine("Deaktivuji:"+deck);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,deck);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Decks> Select() {//3.4
            Console.WriteLine("Selectuji vsechny decky");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_WITH_ALL_COMMENTS);
                using(var reader = db.Select(command)){
                    Collection<Decks> decks = Read_with_comments(reader);
                    return decks;
                }
            }
        }
        public Collection<Decks> Select_best() {//3.4
            Console.WriteLine("Selectuji TOP 3 decky");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_BEST);
                using(var reader = db.Select(command)){
                    Collection<Decks> decks = Read(reader);
                    return decks;
                }
            }
        }
        
        /// OTHERS ///////////////////////////////////////////////////////////// 
        private static Collection<Decks> Read(SqlDataReader reader) {
            var decks = new Collection<Decks>();
            var dt = new CommentsTable();
            while (reader.Read()) {
                var d = new Decks();
                d.deck_id = reader.GetInt32(0);
                d.name = reader.GetString(1);
                d.description = Get_note_from_db(reader[2]);
                d.max_cards_count = reader.GetInt32(3);
                d.cards_count = reader.GetInt32(4);
                d.stars = Get_num_from_db(reader[5]);
                d.player_id = reader.GetInt32(6);
                d.deleted_at =  Get_date_from_db(reader[7]);

                decks.Add(d);
            }
            return decks;
        }
        
        private static Collection<Decks> Read_with_comments(SqlDataReader reader) {
            var decks = new Collection<Decks>();
            while (reader.Read()) {
                var deck_id = reader.GetInt32(0);
                var name = reader.GetString(1);
                var description = Get_note_from_db(reader[2]);
                var max_cards_count = reader.GetInt32(3);
                var cards_count = reader.GetInt32(4);
                var stars = Get_num_from_db(reader[5]);
                var player_id = reader.GetInt32(6);
                var deleted_at =  Get_date_from_db(reader[7]);

                var c_id = Get_num_from_db(reader[8]);
                var c_text = Get_note_from_db(reader[9]);
                var c_added = Get_date_from_db(reader[10]);
                var c_player_id = Get_num_from_db(reader[11]);
                var c_deck_id = Get_num_from_db(reader[12]);
                
                var deck = decks.FirstOrDefault(dd => dd.deck_id == deck_id);
                if (deck == null) {
                    deck = new Decks {
                        deck_id = deck_id,
                        name = name,
                        description = description,
                        max_cards_count = max_cards_count,
                        cards_count = cards_count,
                        stars = stars,
                        player_id = player_id,
                        deleted_at = deleted_at,
                        comments = new Collection<Comments>()
                    };
                    if(c_added != null & c_id != null & c_player_id != null & c_deck_id != null) {
                        var comment = new Comments(c_text, (int) c_player_id, (int) c_deck_id) {
                            comment_id = (int) c_id, added_at = (DateTime) c_added
                        };
                        deck.comments.Add(comment);
                    }
                    decks.Add(deck);
                }
                else {
                    if (c_added != null & c_id != null & c_player_id != null & c_deck_id != null) {
                        var comment = new Comments(c_text, (int) c_player_id, (int) c_deck_id) {
                            comment_id = (int) c_id, added_at = (DateTime) c_added
                        };

                        if (!deck.comments.Contains(comment)) {
                            
                            deck.comments.Add(comment);
                        }
                    }
                }
            }
            return decks;
        }
      
        private static DateTime? Get_date_from_db(object value) {
            return value == DBNull.Value ? (DateTime?) null : Convert.ToDateTime(value); 
        }
        
        private static string Get_note_from_db(object value) {
            return value == DBNull.Value ? null : Convert.ToString(value);
        }
        
        private static int? Get_num_from_db(object value) {
            return value == DBNull.Value ? (int?) null : Convert.ToInt32(value);
        }
        
        private static void Prepare_command(SqlCommand c, Decks d) {
            c.Parameters.Add("@deck_id", SqlDbType.Int).Value = d.deck_id;
       
            c.Parameters.Add("@name", SqlDbType.VarChar, Decks.LEN_ATTR_name).Value = d.name;
            
            c.Parameters.Add("@description", SqlDbType.VarChar, Decks.LEN_ATTR_description);
            if (d.description == null) { c.Parameters["@description"].Value = DBNull.Value; }
            else { c.Parameters["@description"].Value = d.description;}             
           
            c.Parameters.Add("@max_cards_count", SqlDbType.Int).Value = d.max_cards_count;          
           
            c.Parameters.Add("@cards_count", SqlDbType.Int).Value = d.cards_count;          
           
            c.Parameters.Add(new SqlParameter("@stars", SqlDbType.Int));
            if (d.stars == null) { c.Parameters["@stars"].Value = DBNull.Value; }
            else { c.Parameters["@stars"].Value = d.stars;}           
            
            c.Parameters.Add("@player_id", SqlDbType.Int).Value = d.player_id;   
           
            c.Parameters.Add(new SqlParameter("@deleted_at", SqlDbType.DateTime));
            if (d.deleted_at == null) { c.Parameters["@deleted_at"].Value = DBNull.Value; }
            else { c.Parameters["@deleted_at"].Value = d.deleted_at;} 
            
            
        }
        
        public Dictionary<Decks, List<Comments>> zkouska() {
            var decks = Select();
            var comments = new CommentsTable().Select();
            var d_w_c = new Dictionary<Decks, List<Comments>>();
            foreach (var d in decks) {
                
                var cs = new List<Comments>();
                foreach (var c in comments) {
                    if (d.deck_id == c.deck_id) {
                        //     if (decks.Contains(d)) {
                        cs.Add(c);
                    }
                    //}
                }
                d_w_c.Add(d, cs);
            }

            return d_w_c;
        }
    }
}