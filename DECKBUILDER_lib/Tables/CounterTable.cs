using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class CounterTable{
        private const string SQL_INSERT = "INSERT INTO counter (day_stamp) values (@day_stamp)";
        private const string SQL_SELECT = "SELECT day_stamp, decks_created, players_active, cards_added_to_decks, comments_added from counter";
        private const string SQL_DELETE = "DELETE FROM counter where day_stamp=@day_stamp";
        private const string SQL_UPDATE = "UPDATE counter set decks_created=@decks_created, players_active = @players_active, cards_added_to_decks = @cards_added_to_decks, comments_added = @comments_added where day_stamp=@day_stamp";
        
        private Database.Database db;

        public CounterTable() {
            db = new Database.Database();
        }
        
        ///  CRUD ///////////////////////////////////////////////////////////// 
        public int Insert(Counter counter) {//7.1
            Console.WriteLine("Vkladam: "+counter);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, counter);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Counter counter) {//7.3
            Console.WriteLine("Updatuji: "+counter);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, counter);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Counter counter) {//7.2
            Console.WriteLine("Mazu zaznam: "+counter);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,counter);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Counter> Select() {//7.4
            Console.WriteLine("Selectuji vsechny zaznamy countru:");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Counter> counter = Read(reader);
                    return counter;
                }
            }
        }
        
        /// Others ///////////////////////////////////////////////////////////// 
        private static Collection<Counter> Read(SqlDataReader reader) {
            var counters = new Collection<Counter>();
            while (reader.Read()) {
                var c = new Counter();
                
                c.day_stamp = reader.GetDateTime(0);
                c.decks_created = Get_num_from_db(reader[1]);
                c.players_active = Get_num_from_db(reader[2]);
                c.cards_added_to_decks = Get_num_from_db(reader[3]);
                c.comments_added = Get_num_from_db(reader[4]);
                
                counters.Add(c);              
            }
            return counters;
        }
        
        private static int? Get_num_from_db(object value) {
            return value == DBNull.Value ? (int?) null : Convert.ToInt32(value);
        }
        
        private static void Prepare_command(SqlCommand c, Counter co) {
                c.Parameters.Add("@day_stamp", SqlDbType.DateTime).Value = co.day_stamp;
            
                c.Parameters.Add(new SqlParameter("@decks_created", SqlDbType.Int));
                if (co.decks_created == null) { c.Parameters["@decks_created"].Value = DBNull.Value; }
                else { c.Parameters["@decks_created"].Value = co.decks_created.Value;}
                
                c.Parameters.Add(new SqlParameter("@players_active", SqlDbType.Int));
                if (co.players_active == null) { c.Parameters["@players_active"].Value = DBNull.Value; }
                else { c.Parameters["@players_active"].Value = co.players_active.Value;}
                
                c.Parameters.Add(new SqlParameter("@cards_added_to_decks", SqlDbType.Int));
                if (co.cards_added_to_decks == null) { c.Parameters["@cards_added_to_decks"].Value = DBNull.Value; }
                else { c.Parameters["@cards_added_to_decks"].Value = co.cards_added_to_decks.Value;}
                
                c.Parameters.Add(new SqlParameter("@comments_added", SqlDbType.Int));
                if (co.comments_added == null) { c.Parameters["@comments_added"].Value = DBNull.Value; }
                else { c.Parameters["@comments_added"].Value = co.comments_added.Value;}  
            
        }
    }
}