using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Interfaces;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class SetsTable:ITable<Sets>{
        private const string SQL_INSERT = "INSERT INTO sets (SET_CODE, NAME, TYPE, \"YEAR\") values (@set_code, @name, @type, @year)";
        private const string SQL_SELECT = "select set_code, name, type, year, deleted_at from sets";
        private const string SQL_SELECT_ID = "select * from sets where set_code = @set_code";
        private const string SQL_UPDATE = "UPDATE sets SET name=@name, year=@year, type=@type where set_code = @set_code";
        private const string SQL_DELETE = "UPDATE sets SET year=0 where set_code = @set_code";
         private Database.Database db;

        public SetsTable() {
            db = new Database.Database();
        }
        /// NOT CRUD ///////////////////////////////////////////////////////////// 
        public Sets Select(string set_code) {
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_SELECT_ID);
                
                command.Parameters.Add(new SqlParameter("@set_code", SqlDbType.VarChar,Sets.LEN_ATTR_set_code));
                command.Parameters["@set_code"].Value = set_code;
                
                using (var reader = db.Select(command)) {
                    var sets = Read(reader);
                    Sets set = null;

                    if (sets.Count == 1) {set = sets[0];}

                    

                    return set;
                }
            }
        }
        
        /// CRUD ///////////////////////////////////////////////////////////// 
        public int Insert(Sets set) {//8.1
            Console.WriteLine("Vkladam: "+set);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, set);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Sets set) {//8.3
            Console.WriteLine("Updatuji: "+set);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, set);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Sets set) {//8.2
            Console.WriteLine("Deaktivuji:"+set);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,set);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Sets> Select() {//8.4
            Console.WriteLine("Selectuji vsechny sety:");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Sets> sets = Read(reader);
                    return sets;
                }
            }
        }
        
        
        
        /// OTHERS ///////////////////////////////////////////////////////////// 
        private static Collection<Sets> Read(SqlDataReader reader) {
            Collection<Sets> sets = new Collection<Sets>();
            while (reader.Read()) {
                var s = new Sets {
                    set_code = reader.GetString(0),
                    name = reader.GetString(1),
                    type = reader.GetString(2),
                    year = reader.GetInt32(3)
                };


                sets.Add(s);              
            }
            return sets;
        }
        private static void Prepare_command(SqlCommand c, Sets s) {
            c.Parameters.Add("@set_code", SqlDbType.VarChar, Sets.LEN_ATTR_set_code).Value = s.set_code;            
            c.Parameters.Add("@name", SqlDbType.VarChar, Sets.LEN_ATTR_name).Value = s.name;            
            c.Parameters.Add("@type", SqlDbType.VarChar, Sets.LEN_ATTR_type).Value = s.type;            
            c.Parameters.Add("@year", SqlDbType.Int).Value = s.year;
            c.Parameters.Add(new SqlParameter("@deleted_at", SqlDbType.DateTime));
            if (s.deleted_at == null) { c.Parameters["@deleted_at"].Value = DBNull.Value; }
            else { c.Parameters["@deleted_at"].Value = s.deleted_at;}
        }
    }
}