using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class CommentsTable{
        private const string SQL_INSERT = "INSERT INTO comments (text, added, player_id, deck_id) values(@text, @added, @player_id, @deck_id)";
        private const string SQL_UPDATE = "UPDATE comments SET text = @text where comment_id = @comment_id";
        private const string SQL_SELECT = "SELECT comment_id, text, added, player_id, deck_id, deleted_at from comments"; 
        private const string SQL_SELECT_BY_DECK_ID = "SELECT comment_id, text, added, player_id, deck_id, deleted_at from comments where deck_id = @deck_id and deleted_at is null"; 
        private const string SQL_DELETE = "update comments set deleted_at = sysdatetime() where comment_id = @comment_id";
        
        private Database.Database db;

        public CommentsTable() {
            db = new Database.Database();
        }
        public int Add_comment(Comments comment) {
            Console.WriteLine("Vytvarim "+comment);
            using (db.GetConnection) {
                db.Connect();
                using (var cmd = new SqlCommand("add_comment")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    cmd.Parameters.Add("@ret", SqlDbType.Int);
                    cmd.Parameters["@ret"].Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@text", comment.text);
                    cmd.Parameters.AddWithValue("@player_id", comment.player_id);
                    cmd.Parameters.AddWithValue("@deck_id", comment.deck_id);
                    
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tKladné číslo = úspěch při přidávání komentáře:\t "+cmd.Parameters["@ret"].Value);
                    return (int) cmd.Parameters["@ret"].Value;
                }                
            }    
        }
        /// CRUD ///////////////////////////////////////////////////////////// 
        public int Insert(Comments comment) {//4.1
            Console.WriteLine("Vkladam: "+comment);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, comment);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        public int Update(Comments comment) {//4.2
            Console.WriteLine("Updatuji: " + comment);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, comment);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Comments comment) {//4.3
            Console.WriteLine("Mazu zaznam:"+comment);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,comment);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Comments> Select() {
            Console.WriteLine("Selectuji vsechny komentare");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Comments> comments = Read(reader);
                    return comments;
                }
            }
        }
        public Collection<Comments> Select_by_deck_id(int deck_id) {//4.4
            //Console.WriteLine("Selectuji komentare k decku s ID "+deck_id);
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT_BY_DECK_ID);
                
                command.Parameters.Add(new SqlParameter("@deck_id", SqlDbType.Int));
                command.Parameters["@deck_id"].Value = deck_id; 
                
                
                using(var reader = db.Select(command)){
                    Collection<Comments> comments = Read(reader);
                    return comments;
                }
            }
        }
        
        /// OTHERS ///////////////////////////////////////////////////////////// 
        private static Collection<Comments> Read(SqlDataReader reader) {
            var comments = new Collection<Comments>();
            while (reader.Read()) {
                var c = new Comments(
                    reader.GetString(1),//text
                    reader.GetInt32(3),//player
                    reader.GetInt32(4)//deck
                    );
                c.comment_id = reader.GetInt32(0);
                c.added_at = reader.GetDateTime(2);
                c.deleted_at = Get_date_from_db(reader[5]);
                //comment_id, text, added, player_id, deck_id
                
                comments.Add(c);              
            }
            return comments;
        }
        private static DateTime? Get_date_from_db(object value) {
            return value == DBNull.Value ? (DateTime?) null : Convert.ToDateTime(value); 
        }
        private void Prepare_command(SqlCommand c, Comments k) {
            c.Parameters.Add("@comment_id",SqlDbType.Int).Value= k.comment_id;
            
            c.Parameters.Add("@text",SqlDbType.VarChar,Comments.LEN_ATTR_text).Value= k.text;            
           
            c.Parameters.Add("@added", SqlDbType.DateTime).Value=k.added_at;            
            
            c.Parameters.Add("@player_id",SqlDbType.Int).Value=  k.player_id;  
            
            c.Parameters.Add("@deck_id",SqlDbType.Int).Value= k.deck_id;
            
            c.Parameters.Add(new SqlParameter("@deleted_at", SqlDbType.DateTime));
            if (k.deleted_at == null) { c.Parameters["@deleted_at"].Value = DBNull.Value; }
            else { c.Parameters["@deleted_at"].Value = k.deleted_at;}          
        }
    }
}