using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Interfaces;
using DECKBUILDER_lib.Objects;

namespace DECKBUILDER_lib.Tables{
    public class PlayersTable:ITable<Players>{
        //CRUD
        private const string SQL_INSERT = "INSERT INTO players ( login, registered, password, note) VALUES ( @login, @registered, @password, @note)";
        private const string SQL_SELECT = "SELECT player_id, login, registered, password, deleted_at, note FROM players";
        private const string SQL_UPDATE = "UPDATE players SET note=@note, password=@password where player_id=@player_id";
        private const string SQL_DELETE = "UPDATE players SET deleted_at=sysdatetime() where player_id=@player_id";
        
        private const string SQL_SELECT_ID = "SELECT player_id, login, registered, password, deleted_at, note FROM players where player_id=@player_id";
        private const string SQL_SELECT_MOST_VOTING = "select * from players where player_id in( select TOP 1 p.player_id from players p join ratings r on r.player_id = p.player_id group by p.player_id order by count(r.player_id) desc)";
        
        private Database.Database db;
        
        public PlayersTable() {
            db = new Database.Database();
        }

        /// NOT CRUD /////////////////////////////////////////////////////////////
        public int Log_in(string login, string password) {
            Console.WriteLine("Prihlasuji hrace: "+login);
            using (db.GetConnection) {
                db.Connect();
                using (var cmd = new SqlCommand("log_in")) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = db.GetConnection;
                    cmd.Parameters.AddWithValue("@login",login);
                    cmd.Parameters.AddWithValue("@password", password);
                    cmd.Parameters.Add("@ret", SqlDbType.Int);
                    cmd.Parameters["@ret"].Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tJednicka pro uspesne prihlaseni:\t "+cmd.Parameters["@ret"].Value.ToString());
                    return (int) cmd.Parameters["@ret"].Value;
                }                
            }
        }
        public Players Select_most_voting() { //11.3
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_SELECT_MOST_VOTING);
                
                using (var reader = db.Select(command)) {
                    var players = Read(reader);
                    Players player = null;
                    
                    if (players.Count == 1) { player = players[0]; }
                    
                    return player;
                }
            }
        }
        public Players Select(int player_id) {
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_SELECT_ID);
                
                command.Parameters.Add(new SqlParameter("@player_id", SqlDbType.Int));
                command.Parameters["@player_id"].Value = player_id;
                
                using (var reader = db.Select(command)) {
                    var players = Read(reader);
                    Players player = null;
                    
                    if (players.Count == 1) { player = players[0]; }
                    
                    return player;
                }
            }
        }
        
        /// CRUD ///////////////////////////////////////////////////////////// 
        public int Insert(Players player) {//1.1
            Console.WriteLine("Vkladam: "+player);
            using (db.GetConnection) {
                db.Connect();
                var command = db.CreateCommand(SQL_INSERT);
                
                Prepare_command(command, player);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Update(Players player) {//1.2
            Console.WriteLine("Updatuji: "+player);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_UPDATE);
                
                Prepare_command(command, player);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public int Delete(Players player) {//1.3
            Console.WriteLine("Deaktivuji:" + player);
            using(db.GetConnection){
                db.Connect();
                var command = db.CreateCommand(SQL_DELETE);
                
                Prepare_command(command,player);
                
                var ret = db.ExecuteNonQuery(command);
                return ret;
            }
        }
        
        public Collection<Players> Select() {//1.4
            Console.WriteLine("Selectuji vsechny hrace");
            using(db.GetConnection){
                db.Connect();
                SqlCommand command = db.CreateCommand(SQL_SELECT);
                using(var reader = db.Select(command)){
                    Collection<Players> players = Read(reader);
                    return players;
                }
            }
        }
        
        /// OTHERS ///////////////////////////////////////////////////////////// 
        private static Collection<Players> Read(SqlDataReader reader) {
            var players = new Collection<Players>();
            while (reader.Read()) {
                var p = new Players();
                
                p.player_id = reader.GetInt32(0);
                p.login = reader.GetString(1);
                p.registered = reader.GetDateTime(2);
                p.password = reader.GetString(3);
                p.deleted_at = Get_date_from_db(reader[4]);
                p.note = Get_note_from_db(reader[5]);
                
                players.Add(p);              
            }
            return players;
        }

        private static string Get_note_from_db(object value) {
            return value == DBNull.Value ? null : Convert.ToString(value);
        }
        
        private static DateTime? Get_date_from_db(object value) {
            return value == DBNull.Value ? (DateTime?) null : Convert.ToDateTime(value); 
        }
        
        private static void Prepare_command(SqlCommand c, Players p) {
            c.Parameters.Add("@player_id",SqlDbType.Int).Value= p.player_id;
            c.Parameters.Add("@login", SqlDbType.VarChar,Players.LEN_ATTR_login).Value=p.login;
            c.Parameters.Add("@registered",SqlDbType.DateTime).Value= p.registered;         
            c.Parameters.Add("@password",SqlDbType.VarChar,Players.LEN_ATTR_password).Value= p.password;             
            
            c.Parameters.Add(new SqlParameter("@note", SqlDbType.VarChar, Players.LEN_ATTR_note));
            if (p.note == null) { c.Parameters["@note"].Value = DBNull.Value; }
            else { c.Parameters["@note"].Value = p.note;}  
            
            c.Parameters.Add(new SqlParameter("@deleted_at", SqlDbType.DateTime));
            if (p.deleted_at == null) { c.Parameters["@deleted_at"].Value = DBNull.Value; }
            else { c.Parameters["@deleted_at"].Value = p.deleted_at;}  
        }
        
    }
}