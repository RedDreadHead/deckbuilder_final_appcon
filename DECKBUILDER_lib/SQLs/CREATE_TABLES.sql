CREATE TABLE Players (
                       player_id integer     IDENTITY(1,1) not null ,
                       login VARCHAR(50)      not null,
                       registered date         not null,
                       password VARCHAR(50)   not null,
                       deleted_at date             null,
                       note VARCHAR(250)          null,

                       constraint player_PK primary key (player_id),

                       constraint unique_login unique (login),
                       constraint players_check_deleted_soon check(registered <= deleted_at)
);
create table Decks
(
  deck_id   integer       IDENTITY(1,1)  not null,
  name varchar(50)          not null,
  description varchar(250)     null,
  max_cards_count integer   default 60 not null,
  cards_count integer       default 0 not null,
  stars integer                 null,
  player_id integer         not null,
  deleted_at datetime null,

  constraint deck_PK primary key (deck_id),
  constraint deck_player_FK foreign key (player_id) references players(player_id),

  constraint decks_check_stars check (stars between 0 and 5),
  constraint check_cards_count  check(cards_count <= max_cards_count)
);
create table Comments (
                        comment_id integer    IDENTITY(1,1)    not null,
                        text varchar(250)        not null,
                        added date                not null,
                        player_id integer         not null,
                        deck_id integer           not null,
                        deleted_at datetime null,

                        constraint comment_PK primary key (comment_id),
                        constraint comment_deck_fk foreign key (deck_id) references decks(deck_id),
                        constraint comment_player_fk foreign key (player_id) references players(player_id)

);
create table Sets (
                    set_code varchar(5)      not null,
                    name varchar(50)         not null,
                    type varchar(50)         not null,
                    year integer              not null,
                    deleted_at Datetime null,

                    constraint set_PK primary key (set_code),

                    constraint set_type check (type in ('expansion','core','reprint','box','un-set','from the vault',
                                                        'premium deck','duel deck','starter','commander','planechase',
                                                        'archenemy','promo','vanguard','masters','compilation','supplemental'))
);
create table Cards (
                     card_id integer           not null,
                     name varchar(250)        not null,
                     type varchar(50)         not null,
                     color varchar(50)        not null,
                     artist varchar(250)          null,
                     image varchar(250)           null,
                     cmc integer               not null,
                     rarity integer            not null,
                     power integer                 null,
                     toughness integer             null,
                     set_code varchar(5)      not null,
                     deleted_at DateTime null,


                     constraint card_PK primary key (card_id),
                     constraint card_set_FK foreign key (set_code) references sets(set_code),

                     constraint card_check_type check (type in ('artifact', 'creature', 'enchantment', 'instant', 'land', 'planeswalker', 'sorcery')),
                     constraint card_check_color check (color in ('blue', 'white', 'black', 'green', 'red')),
                     constraint card_check_img check ((image is not null and artist is not null) or (image is null and artist is null)),
                     constraint card_check_creature check ((type like 'creature' and power is not null and toughness is not null)
                       or
                                                           type not like 'creature'),
                     constraint card_check_rarity check (rarity between 0 and 3) --common, uncommon, rare and mythic rare
);
create table Changes (
                       change_type char(1)       not null,
                       card_id integer           not null,
                       comment_id integer        not null,
                       count integer   default 1 not null,

                       constraint change_PK primary key (card_id, comment_id) ,
                       constraint change_card_FK foreign key (card_id) references cards(card_id),
                       constraint change_comment_FK foreign key (comment_id) references comments(comment_id),

                       constraint change_check_type check (change_type in ('A', 'D')) --add/delete
);



create table Card_deck(
                        count integer             not null,
                        card_id integer           not null,
                        deck_id integer           not null,

                        constraint card_deck_PK primary key (card_id, deck_id),
                        constraint card_deck_deck_FK foreign key (deck_id) references decks(deck_id),
                        constraint card_deck_card_FK foreign key (card_id) references cards(card_id),

                        constraint cd_check_count_60 check (count between 0 and 60)
);



create table Ratings (
                       stars integer not null,
                       player_id integer not null,
                       deck_id integer not null,

                       constraint rating_PK primary key (player_id, deck_id),
                       constraint rating_player_FK foreign key (player_id) references players(player_id),
                       constraint rating_deck_FK foreign key (deck_id) references decks(deck_id),

                       constraint ratings_check_stars check (stars between 0 and 5)
);
create table Ranks (
                     stars integer not null,
                     rank_name VARCHAR(50) not null,

                     constraint rank_PK primary key (stars),
                     constraint rank_check_stars check (stars between 0 and 5)
);
create table Counter(
                      day_stamp date not null,
                      decks_created integer null,
                      players_active integer null,
                      cards_added_to_decks integer null,
                      comments_added integer null,

                      constraint counter_PK primary key (day_stamp)
);

