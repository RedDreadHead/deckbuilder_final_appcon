insert into ranks (stars, rank_name) values (0, 'waste');
insert into ranks (stars, rank_name) values (1, 'decent');
insert into ranks (stars, rank_name) values (2, 'passable');
insert into ranks (stars, rank_name) values (3, 'useful');
insert into ranks (stars, rank_name) values (4, 'praiseworthy');
insert into ranks (stars, rank_name) values (5, 'TITBFD');

insert into sets (set_code, name, type, year) values ('LEA', 'Limited Edition Alpha', 'core', 1993)
insert into sets (set_code, name, type, year) values ('LEG', 'Legends', 'expansion', 1994)
insert into sets (set_code, name, type, year) values ('10E', 'Tenth Edition', 'core', 2007)
insert into sets (set_code, name, type, year) values ('SOM', 'Scars of Mirrodin', 'expansion', 2010)
insert into sets (set_code, name, type, year) values ('AVR', 'Avacyn Restored', 'expansion', 2012)
insert into sets (set_code, name, type, year) values ('M13', 'Magic 2013', 'core', 2012)
insert into sets (set_code, name, type, year) values ('THS', 'Therosa', 'expansion', 2013)
insert into sets (set_code, name, type, year) values ('DGM', 'Dragon''s maze', 'expansion', 2013)
insert into sets (set_code, name, type, year) values ('VMA', 'Vintage Masters', 'compilation', 2014)
insert into sets (set_code, name, type, year) values ('M15', 'Magic 2015', 'core', 2014)
insert into sets (set_code, name, type, year) values ('C14', 'Commander 2014', 'box', 2014)
insert into sets (set_code, name, type, year) values ('C15', 'Commander 2015', 'box', 2015)
insert into sets (set_code, name, type, year) values ('EMN', 'Eldritch Moon', 'expansion', 2016)
insert into sets (set_code, name, type, year) values ('EMA', 'Eternal Masters', 'compilation', 2016)
insert into sets (set_code, name, type, year) values ('C16', 'Commander 2016', 'box', 2016)
insert into sets (set_code, name, type, year) values ('V17', 'From the Vault: Transform', 'box', 2017)
insert into sets (set_code, name, type, year) values ('IMA', 'Iconic Masters', 'compilation', 2017)
insert into sets (set_code, name, type, year) values ('AER', 'Aether Revolt', 'expansion', 2017)
insert into sets (set_code, name, type, year) values ('E01', 'Archenemy: Nicol Bolas', 'supplemental', 2017)
insert into sets (set_code, name, type, year) values ('C17', 'Commander 2017', 'box', 2017)
insert into sets (set_code, name, type, year) values ('UMA', 'Ultimate Masters', 'compilation', 2018)
insert into sets (set_code, name, type, year) values ('M19', 'Core Set 2019', 'core', 2018)

insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (461177, 'Plains', 'land', 'white', 'Jesper Myrfors', 'https://img.scryfall.com/cards/large/en/lea/286.jpg?1525123704',0,0, 'LEA' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (461189, 'Forest', 'land', 'green', 'Jonas De Ro', 'https://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=461189&type=card',0,0, 'LEA' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (461183, 'Swamp', 'land', 'black', 'Titus Lunter', 'https://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=461183&type=card',0,0, 'LEA' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (461180, 'Island', 'land', 'blue', 'Titus Lunter', 'https://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=461180&type=card',0,0, 'LEA' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (461186, 'Mountain', 'land', 'red', 'Titus Lunter', 'https://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=461186&type=card',0,0, 'LEA' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (3, 'Black Lotus', 'artifact', 'black', 'Christopher Rust', 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=3&type=card',0,0, 'LEA' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (450229, 'Ajani''s Influence', 'sorcery', 'white', 'Sidharth Chaturvedi', 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=450229&type=card',4,3, 'M19' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (447137, 'Aegis of the Heavens', 'instant', 'white', 'Anthony Palumbo', 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=447137&type=card',2,1, 'M19' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code) values (383180, 'Legendary Planeswalker — Ajani', 'planeswalker', 'white', 'Chris Rahn', 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=383180&type=card',4,2, 'M15' )
insert into cards(card_id, name, type, color, artist, image, cmc, rarity, set_code, power, toughness) values (383180, 'Ajani''s Primedate', 'creature', 'white', 'Svetlin Velinov', 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=383181&type=card',2,1, 'M15',2,2 )
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (447137, 'Aegis of the Heavens', 2,'M19','white','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=447137&type=card','Anthony Palumbo',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (450228, 'Ajani, Wise Counselor', 5,'M19','white','planeswalker',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=450228&type=card','Eric Deschamps',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (447142, 'Ajani''s Welcome', 1,'M19','white','enchantment',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=447142&type=card','Eric Deschamps',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (447140, 'Ajani''s Last Stand', 4,'M19','white','enchantment',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=447140&type=card','Slawomir Maniak',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (439341, 'Nissa, Vastwood Seer', 3,'V17','green','creature',2,2,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=439341&type=card','Wesley Burt',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (373535, 'Thassa, God of the Sea', 3,'THS','blue','creature',5,5,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=373535&type=card','Jason Chan',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (212249, 'Skithiryx, the Blight Dragon', 5,'SOM','black','creature',4,4,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=212249&type=card','Chippy',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (253670, 'Primordial Hydra', 2,'M13','green','creature',0,0,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=253670&type=card','Aleksi Briclot',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (373556, 'Purphoros, God of the Forge', 4,'THS','red','creature',6,5,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=373556&type=card','Eric Deschamps',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (369031, 'Ral Zarek', 4,'DGM','red','planeswalker',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=369031&type=card','Eric Deschamps',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (433082, 'Frontier Siege', 4,'C17','green','enchantment',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=433082&type=card','James Ryman',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (401931, 'Kiora, Master of the Depths', 4,'BFZ','green','planeswalker',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=401931&type=card','Jason Chan',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (451026, 'Return to Dust', 4,'C18','white','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=451026&type=card','Wayne Reynolds',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (438674, 'Sheoldred, Whispering One', 7,'IMA','black','creature',6,6,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=438674&type=card','Jana Schirmer & Johannes Voss',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (394633, 'Narset Transcendent', 4,'DTK','blue','planeswalker',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=394633&type=card','Magali Villeneuve',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (1624, 'Land Tax', 1,'LEG','white','enchantment',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=1624&type=card','Brian Snõddy',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (240133, 'Temporal Mastery', 7,'AVR','blue','sorcery',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=240133&type=card','Franz Vohwinkel',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (433076, 'Utvara Hellkite', 8,'C17','red','creature',6,6,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=433076&type=card','Mark Zug',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (414495, 'Tamiyo, Field Researcher', 4,'EMN','green','planeswalker',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=414495&type=card','Tianhua X',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (423696, 'Baral''s Expertise', 5,'AER','blue','sorcery',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=423696&type=card','Todd Lockwood',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (438755, 'Vorinclex, Voice of Hunger', 8,'IMA','green','creature',7,6,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=438755&type=card','Karl Kopinski',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (438577, 'Avacyn, Angel of Hope', 8,'IMA','white','creature',8,8,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=438577&type=card','Jason Chan',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (405314, 'Mystic Confluence', 5,'C15','blue','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=405314&type=card','Kieran Yanner',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (438614, 'Cryptic Command', 4,'IMA','blue','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=438614&type=card','Jason Rainville',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (405158, 'Brainstorm', 1,'C15','blue','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=405158&type=card','Willian Murai',0)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (442130, 'Lightning Bolt', 1,'A25','red','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=442130&type=card','Christopher Moeller',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (413599, 'Jace, the Mind Sculptor', 4,'EMA','blue','planeswalker',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=413599&type=card','Jason Chan',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (430551, 'Grand Abolisher', 2,'E01','white','creature',2,2,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=430551&type=card','Eric Deschamps',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (376436, 'Oloro, Ageless Ascetic', 6,'C13','black','creature',4,5,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=376436&type=card','Eric Deschamps',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (456667, 'Snapcaster Mage', 2,'UMA','blue','creature',2,1,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=456667&type=card','Ryan Alexander Lee',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (60, 'Demonic Tutor', 2,'LEA','black','sorcery',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=60&type=card','Douglas Shuler',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (413591, 'Force of Will', 5,'EMA','blue','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=413591&type=card','Terese Nielsen',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (433008, 'Swords to Plowshares', 1,'C17','white','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=433008&type=card','Terese Nielsen',1)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (420645, 'Atraxa, Praetors'' Voice', 4,'C16','black','creature',4,4,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=420645&type=card','Victor Adame Minguez',3)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (389477, 'Cyclonic Rift', 2,'C14','blue','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=389477&type=card','Chris Rahn',2)
insert into cards(card_id, name, cmc, set_code, color, type, power, toughness,image,artist,rarity) values (383006, 'Mana Drain', 2,'VMA','blue','instant',null,null,'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=383006&type=card','Mike Bierek',3)

insert into players (login, registered, password, deleted_at, note) values('pepicek',sysdatetime(),'milujuanicku',null,null)
insert into players (login, registered, password, deleted_at, note) values('anicka',sysdatetime(),'pepiceksmrdi',null,null)
insert into players (login, registered, password, deleted_at, note) values('kokrhel',sysdatetime(),'jsemnejlepsi',null,null)

insert into decks (name, description, max_cards_count, cards_count, stars, player_id, deleted_at) values ('Nejlepší deck ever','Tohle je supr deck',60,0,null,1,null)
insert into decks (name, description, max_cards_count, cards_count, stars, player_id, deleted_at) values ('Uplně nejlepší deck ever','Tohle je supr čupr deck',60,0,null,2,null)
insert into decks (name, description, max_cards_count, cards_count, stars, player_id, deleted_at) values ('Extra Ultra Mega Deck',null,60,0,null,3,null)

begin
  declare @d int
  exec add_card_to_deck 461189, 3, 10, @d
  exec add_card_to_deck 439341, 3, 4, @d
  exec add_card_to_deck 438755, 3, 4, @d
  exec add_card_to_deck 433082, 3, 4, @d
  exec add_card_to_deck 253670, 3, 4, @d
  exec add_card_to_deck 461180, 3, 10, @d
  exec add_card_to_deck 456667, 3, 4, @d
  exec add_card_to_deck 438614, 3, 4, @d
  exec add_card_to_deck 423696, 3, 4, @d
end