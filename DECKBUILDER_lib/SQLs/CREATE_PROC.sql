--ohodnoceni decku

create or alter procedure p_rate @stars int, @player_id int, @deck_id int
as
begin
    begin try
        declare @avg_rating numeric
        begin transaction
            if @stars not between 0 and 5
                begin
                    SELECT CAST(0 AS BIT)
                end
            else
                begin
                    if(select count(stars) from ratings
                       where deck_id = @deck_id and player_id = @player_id) = 0
                        insert into Ratings (stars, player_id, deck_id) values (@stars, @player_id, @deck_id)
                    else
                        update Ratings set stars = @stars where player_id = @player_id and deck_id = @deck_id
                    select @avg_rating = avg(stars) from ratings where deck_id = @deck_id group by deck_id
                    update decks set stars =round(@avg_rating,0) where deck_id = @deck_id
                    SELECT CAST(1 AS BIT)
                end
        commit
    end try
    begin catch
        rollback
    end catch
end
--vytvoreni decku

create or alter procedure create_deck @name varchar(50), @description varchar(250), @player_id int , @ret int output
as
begin
set @ret = -1
declare @is_in_counter integer = 0
exec is_in_counter @is_in_counter output
insert into decks (name, description, max_cards_count, cards_count, player_id) values (@name, @description, 60, 0, @player_id)
if(@is_in_counter = 1)
  begin
    if((select decks_created from counter where day_stamp = convert (date, sysdatetime()))is null)
      update counter set decks_created = 1 where day_stamp = (CONVERT( date, SYSDATETIME()));
    else
      update counter set decks_created += 1 where day_stamp = (CONVERT( date, SYSDATETIME()));
  end
else
  insert into counter (day_stamp, decks_created) values ((CONVERT( date, SYSDATETIME())), 1)
set @ret = 1
end
--přidání komentáře
create or alter procedure add_comment @text varchar(250), @player_id int, @deck_id int, @ret int output as
begin
  set @ret = -1

  declare @is_in_counter integer = 0
  exec is_in_counter @is_in_counter output
  insert into comments (text, added, player_id, deck_id) values (@text,(CONVERT( date, SYSDATETIME())),@player_id, @deck_id )

  if(@is_in_counter = 1)
    begin
      if((select comments_added from counter where day_stamp = convert (date, sysdatetime()))is null)
        update counter set comments_added = 1 where day_stamp = (CONVERT( date, SYSDATETIME()));
      else
        update counter set comments_added += 1 where day_stamp = (CONVERT( date, SYSDATETIME()));
    end
  else
    insert into counter (day_stamp, comments_added) values ((CONVERT( date, SYSDATETIME())), 1)
  set @ret = 1
end

  --"prihlášení"
create or alter procedure log_in @login varchar(50), @password varchar(50), @ret int output
as
begin
declare @is_in_counter integer = 0
declare @tmp integer = -1
exec is_in_counter @is_in_counter output
select @tmp = count(*) from players where login = @login and password = @password
if(@tmp > 0)
  begin
    set @ret = 1
    if(@is_in_counter = 1)
      begin
        if((select players_active from counter where day_stamp = convert (date, sysdatetime()))is null)
          update counter set players_active = 1 where day_stamp = (CONVERT( date, SYSDATETIME()));
        else
          update counter set players_active += 1 where day_stamp = (CONVERT( date, SYSDATETIME()));
      end
    else
      insert into counter (day_stamp, players_active) values ((CONVERT( date, SYSDATETIME())), 1)
  end
end


        --------------------------MANIPULACE S KATRAMI V BALÍČKU -------

  --Vložení karty do balíčku
create or alter procedure add_card_to_deck @card_id int, @deck_id int, @count int, @ret int output
as
declare @is_enough_space integer = 0;
declare @c_in_deck integer = null;
declare @is_land integer = 0;
declare @is_today_counter integer = 0;

begin
  BEGIN TRY
      begin transaction
          exec is_enough_space @deck_id, @count, @is_enough_space output;
          select @c_in_deck = sum(count) from card_deck where deck_id = @deck_id and card_id = @card_id;
          exec is_land @card_id, @is_land output;
          exec is_in_counter @is_today_counter output;

          if(@is_enough_space <> 0) --je dost míst?
              begin
                  print 'Je dost místa'
                  if(@c_in_deck IS NULL) --ješě v decku není?
                      begin
                          print 'ještě v decku není'
                          if((@is_land = 1)or(@count between 0 and 4))-- land nebo méně než 4 karty?
                              begin
                                  print 'vyhovuje'
                                  insert into card_deck (count, card_id, deck_id) values (@count, @card_id, @deck_id);
                                  set @ret = 1

                                  if(@is_today_counter = 1)
                                      begin
                                          if((select cards_added_to_decks from counter where day_stamp = convert (date, sysdatetime()))is null)
                                              update counter set cards_added_to_decks = @count where day_stamp = (CONVERT( date, SYSDATETIME()));
                                          else
                                              update counter set cards_added_to_decks += @count where day_stamp = (CONVERT( date, SYSDATETIME()));
                                      end
                                  else
                                      insert into counter (day_stamp, cards_added_to_decks) values ((CONVERT( date, SYSDATETIME())), @count)
                                  update decks set cards_count += @count where deck_id = @deck_id
                              end
                          else
                              begin
                                  set @ret = -2
                                  print 'Karta musí být LAND nebo méně než 4x'
                              end
                      end
                  else
                      begin
                          print 'už je v decku'
                          if((@is_land = 1) or (@count + @c_in_deck between 0 and 4))
                              begin
                                  print 'vyhovuje: '
                                  update card_deck set count += @count where card_id = @card_id and deck_id = @deck_id;

                                  if(@is_today_counter = 1)
                                      begin
                                          if((select cards_added_to_decks from counter where day_stamp = convert (date, sysdatetime()) )is null)
                                              update counter set cards_added_to_decks = @count where day_stamp = (CONVERT( date, SYSDATETIME()));
                                          else
                                              update counter set cards_added_to_decks += @count where day_stamp = (CONVERT( date, SYSDATETIME()));
                                      end
                                  else
                                      insert into counter (day_stamp, cards_added_to_decks) values ((CONVERT( date, SYSDATETIME())), @count)
                                  print 'po countru?'
                                  update decks set cards_count += @count where deck_id = @deck_id
                                  set @ret = 1

                              end
                          else
                              begin
                                  print 'nevyhovuje'
                              end
                      end
                  commit
              end
          else
              begin
                  set @ret = -1
                  print 'V decku neni dost mista'
              end
  END TRY
  BEGIN CATCH
      ROLLBACK
      SET @ret = -2
  end catch
end

--Odebrání karty z balíčku
create or alter procedure delete_card_from_deck @card_id int, @deck_id int, @count int, @ret int output
as
declare @c_in_deck integer = null;
begin
  begin try
      BEGIN TRANSACTION
          select @c_in_deck = sum(count) from card_deck where deck_id = @deck_id and card_id = @card_id;
          if((@c_in_deck is null)or(@c_in_deck - @count < 0))
              begin
                  set @ret = -1 --nemůže z decku smazat něco, co tam není
              end
          else
              begin
                  if(@c_in_deck = @count)
                      begin
                          delete from card_deck where card_id = @card_id and deck_id = @deck_id
                          set @ret = 2
                      end
                  else
                      begin
                          update card_deck set count = count-@count where card_id = @card_id and deck_id = @deck_id
                          set @ret = 1
                      end
              end
      COMMIT
  end try
  begin catch
      set @ret=-2
      ROLLBACK
  end catch
end

--aplikace zmeny
    create or alter procedure apply_change @card_id int, @comment_id int, @count int, @change_type varchar(1), @ret int output
    as
    declare @d integer = 0
    declare @deck_id integer = 0
    begin
      select @deck_id = deck_id from Comments where comment_id = @comment_id
      if(@change_type like 'A')
        begin
          exec add_card_to_deck @card_id, @deck_id, @count, @d output
        end
      else
        begin
          exec delete_card_from_deck @card_id, @deck_id, @count, @d output
        end
      set @ret = @d
    end
    select * from card_deck
    begin
      declare @d integer = 0
      exec apply_change 1,1,1,'D',@d output
      print @d
    end
------------------------------POMOCNÉ FUNKCE--------------
  --Ověření typu 'land'
  create or alter procedure is_land @card_id numeric, @ret integer output
  as begin
    if((select cards.type from cards where card_id = @card_id) like 'land')
      begin
        set @ret = 1
        SELECT CAST(1 AS BIT)
      end
    else
      begin
        set @ret = 0
        SELECT CAST(0 AS BIT)
      end
  end
    --Ověření existence záznamu pro dnešní den
    create or alter procedure is_in_counter @ret int output as
    begin
      SET DATEFORMAT dmy;
      if((select count(day_stamp) from counter where day_stamp = (CONVERT( date, SYSDATETIME()))) = 1)

        set @ret = 1
      else
        set @ret = 0

    end;

    begin
      declare @d integer = 0
      exec is_in_counter @d output
      print @d
    end

      --Spočítání konkrétní karty v balíčku
      create or alter procedure count_card_in_deck
        @card_id integer,
        @deck_id integer,
        @ret integer output
      as
      DECLARE @COUNT INTEGER
      begin
        select @COUNT = count from Card_deck where card_id = @card_id and deck_id = @deck_id
        set @ret = @count

      end;

      begin
        declare @d integer = 0
        exec count_card_in_deck 1,1,@d output
        print @d
      end

        --Spočítání karet v balíčku

        create or alter procedure count_cards_in_deck @deck_id int, @ret int OUTPUT
        as
        begin
          set @ret = (select sum(count) from card_deck where deck_id = @deck_id)
          --set @ret =  @b
          select cast(@ret as int)
        end


        select sum(count) from card_deck where deck_id = 1

        begin
          declare @d integer = 0
          exec count_cards_in_deck 1,@d output
          print @d
        end

--Zjištění dostatku místa v decku k vložní konkrétního počtu karet
          create or alter procedure is_enough_space
            @deck_id integer, @count integer, @ret integer output
          as
          declare @max integer
          declare @sum integer = 0
          begin
            exec  count_cards_in_deck @deck_id, @sum output
            select @max = max_cards_count from decks where deck_id = @deck_id
            if((@count + @sum )> @max )
              set @ret = 0
            else
              set @ret = 1

          end

          begin
            declare @d integer = 0
            exec is_enough_space 1,2,@d output
            print @d
          end