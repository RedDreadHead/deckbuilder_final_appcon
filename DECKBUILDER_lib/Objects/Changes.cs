using System.Diagnostics.Tracing;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Changes:IObject{
        public string change_type { get; set; }
        public int card_id { get; set; }
        public int comment_id { get; set; }
        public int count { get; set; }


        public Changes(string type, int card, int comm, int count) {
            this.change_type = type;
            this.card_id = card;
            this.comment_id = comm;
            this.count = count;
            
        }

        public override string ToString() {
            return "\tChange on "+count+" card(s) with id " + card_id + " in comment " + comment_id + " is type: " + change_type;
        }
    }
}