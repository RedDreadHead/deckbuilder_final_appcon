using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Ranks:IObject{
        
        public int stars { get; set; }
        public string rank_name { get; set; }

        public static int LEN_ATTR_rank_name = 50;

        public Ranks(string name, int stars) {
            this.stars = stars;
            this.rank_name = name;
        }
        public override string ToString() {
            return "\tRank "+stars+" = "+rank_name;
        }
    }
}