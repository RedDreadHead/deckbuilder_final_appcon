using System;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Comments:IObject{
        public int comment_id { get; set; }
        public string text { get; set; }
        public DateTime added_at { get; set; }
        public int player_id { get; set; }
        public int deck_id { get; set; }  
        public DateTime? deleted_at { get; set; }
        
        public static int LEN_ATTR_text = 250;

        public Comments() {//tmp
            this.comment_id = 6;
            this.text = "~";
            this.added_at = DateTime.Now;
            this.player_id = 2;
            this.deck_id = 3;
        }
        public Comments(string text, int player, int deck) {
            this.text = text;
            this.added_at = DateTime.Now;
            this.player_id = player;
            this.deck_id = deck;
        }
        public override string ToString() {
            return "\tComment: " + text + ", author: "+player_id+", in deck:"+deck_id+", added: " + added_at+", deleted: "+deleted_at.HasValue;
        }
    }
}