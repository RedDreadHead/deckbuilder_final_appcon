using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Decks:IObject{
        
        public int deck_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int max_cards_count { get; set; }
        public int cards_count { get; set; }
        public int? stars { get; set; }
        public int player_id { get; set; }
        public DateTime? deleted_at{ get;  set; }
        
        public Collection<Comments> comments{get;set;}

        public static int LEN_ATTR_name = 50;
        public static int LEN_ATTR_description = 250;

        public Decks(int owner_id) {//tmp
            name = "D" + DateTime.Now.ToString("HHmmssfff");
            max_cards_count = 60;
            player_id = owner_id;
            
        }
        public Decks() { //tmp
            name = "D" + DateTime.Now.ToString("HHmmssfff");
            max_cards_count = 60;
            player_id = 1;
            
        }
        public override string ToString() {
            var ret =  "\tDeck " + name + "= id: " + deck_id + ", cards: " + cards_count+", owner ID: "+player_id+", deleted: "+deleted_at.HasValue + ", comments:\n";
            if (comments != null) {
                foreach (var c in comments) {
                    ret += "\t" + c + "\n";
                }
            }
           
            return ret;
        }
    }
}