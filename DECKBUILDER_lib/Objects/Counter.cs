using System;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Counter:IObject{
        public DateTime day_stamp { get; set; }
        public int? decks_created { get; set; }
        public int? players_active { get; set; }
        public int? cards_added_to_decks { get; set; }
        public int? comments_added { get; set; }

        public Counter() {
            day_stamp = DateTime.Today;
        }
        public override string ToString() {
            return "\tDay "+day_stamp+"= decks created: "+decks_created+", cards added: "+cards_added_to_decks+", players active: "+players_active+", comments added: "+comments_added;
        }
    }
}