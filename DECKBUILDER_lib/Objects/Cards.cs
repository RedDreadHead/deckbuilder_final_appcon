using System;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Cards:IObject{
        public int card_id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string color { get; set; }
        public string artist { get; set; }
        public string image { get; set; }
        public int cmc { get; set; }
        public int rarity { get; set; } 
        public int? power { get; set; }
        public int? toughness { get; set; }
        public string set_code { get; set; }
        public DateTime? deleted_at { get; set; }
        

        public static int LEN_ATTR_name = 250;
        public static int LEN_ATTR_type = 50;
        public static int LEN_ATTR_color = 50;
        public static int LEN_ATTR_artist = 250;
        public static int LEN_ATTR_image = 250;
        public static int LEN_ATTR_set_code = 5;

        public Cards() { //tmp
            this.card_id = Convert.ToInt32(DateTime.Now.ToString("HHmmssfff"));
            this.name = "C" + DateTime.Now.ToString("HHmmssfff");
            this.cmc = 0;
            this.set_code = "37759";
            this.color = "black";
            this.type = "creature";
            this.power = 0;
            this.toughness = 0;
            this.image = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=439477&type=card";
            this.artist = "Artistic Artist";
            this.rarity = 0;
        }

        public Cards(string name, int cmc, string set_code, string color, string type, int power,
            int toughness, string image, string artist, int rarity) {
            this.card_id = Convert.ToInt32(DateTime.Now.ToString("HHmmssfff"));
            this.name = name;
            this.cmc = cmc;
            this.set_code = set_code;
            this.color = color;
            this.type = type;
            this.power = power;
            this.toughness = toughness;
            this.image = image;
            this.artist = artist;
            this.rarity = rarity;

        }

        public override string ToString() {
            return "\tCard "+name+" = id: "+card_id+", type: "+type+", color: "+color+", cmc: "+cmc+", deleted: "+deleted_at.HasValue;
        }
        
    }
}