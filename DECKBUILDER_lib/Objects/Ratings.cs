using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Ratings:IObject{
        
        public int stars { get; set; }
        public int player_id { get; set; }
        public int deck_id { get; set; }
        
        public Ratings(int ss, int pi, int di) {
            stars = ss;
            player_id = pi;
            deck_id = di;
        }

        public override string ToString() {
            return "\tPlayer "+player_id+" rated deck "+deck_id+"with "+stars;
        }
    }
}