using System;
using System.Collections.Generic;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Players:IObject{
        public int player_id {get; set;}
        public string login { get; set; }
        public DateTime registered { get; set; }
        public string password { get; set; }
        public DateTime? deleted_at { get; set; }
        
        public string note { get; set; }
        
        //public List<Decks> decks { get; set; }
        
        public Players(string l) { //tmp
            login = l;
            registered = DateTime.Today;
            password = "*****";
            note = null;
            deleted_at = null;
            

        }
        public Players() { //tmp
            login = "P"+DateTime.Now.ToString("HHmmssfff");
            registered = DateTime.Today;
            password = "**********";
            note = null;
            deleted_at = null;
            

        }
        
        
        public static int LEN_ATTR_login = 50;
        public static int LEN_ATTR_password = 50;
        public static int LEN_ATTR_note = 250;
        
        public override string ToString() {
              
            return "\tPlayer " + login + " = registered: " + registered + ", password: *very secret*, note: " + note + ", deleted: "+deleted_at.HasValue;
        }
        
    }
}