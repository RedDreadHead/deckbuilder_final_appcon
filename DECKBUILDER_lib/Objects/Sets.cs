using System;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Sets:IObject{
        public string set_code { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int year { get; set; }
        public DateTime? deleted_at { get; set; }

        public static int LEN_ATTR_set_code = 5;
        public static int LEN_ATTR_name = 50;
        public static int LEN_ATTR_type = 50;

        public Sets() {
            name = "S"+DateTime.Now.ToString("HHmmssfff");
            set_code = DateTime.Now.ToString("ssfff");
            type = "expansion";
            year = 42;
        }

        public override string ToString() {
            return "\tSet " + name + " = id: " + set_code + ", type: " + type + ", year: " + year+", deleted: "+deleted_at.HasValue;
        }
        
    }
}