using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Objects{
    public class Card_deck:IObject{
        public int count { get; set; }
        public int card_id { get; set; }
        public int deck_id { get; set; }
        

        public Card_deck(int count, int deck_id, int card_id) {
            this.count = count;
            this.deck_id = deck_id;
            this.card_id = card_id;
        }
        
        public override string ToString() {
            return "\tCard " + card_id + " is in deck " + deck_id+ "\t"+count+" times.";
        }
    }
}