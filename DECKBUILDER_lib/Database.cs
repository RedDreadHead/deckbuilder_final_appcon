using System;
using System.Data;
using System.Data.SqlClient;
using DECKBUILDER_lib.Interfaces;

namespace DECKBUILDER_lib.Database{
    public class Database:IDatabase{
        static string conStr = "Data Source=dbsys.cs.vsb.cz\\STUDENT;Initial Catalog=zur0037;User ID=zur0037;Password=xy9nfTBFTZ";

       private SqlConnection my_connection { get; set; }
       private SqlTransaction my_transaction { get; set; }

       public Database() {
           my_connection = new SqlConnection(conStr);
       }
       private bool Connect(String con_string)
       {
           if (my_connection.State != ConnectionState.Open)
           {   my_connection.ConnectionString = con_string;
               my_connection.Open();
           }
           return true;
       }
        public SqlConnection Connect() {
            if (my_connection.State != ConnectionState.Open) {
                Connect(conStr);
            }
            return my_connection;
        }

        

        public void Close() {
            my_connection.Close();
        }
        
        public bool IsConnectionOpen() {
            if (my_connection != null) {
                return true;
            }
            return false;
        }

        public SqlConnection GetConnection => my_connection;

        public void BeginTransaction() {
            my_transaction = my_connection.BeginTransaction(IsolationLevel.Serializable);
        }

        public void EndTransaction() {
            my_transaction.Commit();
            Close();
        }

        public void Rollback() {
            my_transaction.Rollback();
        }
        public int ExecuteNonQuery(SqlCommand command)
        {
            int row_number = 0;
            try
            {
                command.Prepare();
                row_number = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Close();
            }
            return row_number;
        }
        
        public SqlCommand CreateCommand(string str_command) {
            SqlCommand command = new SqlCommand(str_command, my_connection);
            if (my_transaction != null) {
                command.Transaction = my_transaction;
            }

            return command;
        }
        
        public SqlDataReader Select(SqlCommand command)
        {
            command.Prepare();
            SqlDataReader sql_reader = command.ExecuteReader();
            return sql_reader;
        }

        public static void Fill() {
            
        }
        
    }
}