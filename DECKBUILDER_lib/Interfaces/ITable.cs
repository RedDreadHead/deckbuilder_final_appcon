using System.Collections.ObjectModel;

namespace DECKBUILDER_lib.Interfaces{
    public interface ITable<T> where T:IObject{
        int Insert(T entity);
        int Update(T entity);
        int Delete(T entity);
        Collection<T> Select();
    }
}