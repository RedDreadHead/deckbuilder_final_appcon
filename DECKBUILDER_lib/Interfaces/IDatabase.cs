using System.Data.SqlClient;

namespace DECKBUILDER_lib.Interfaces{
    public interface IDatabase{
        SqlConnection Connect();
        void Close();
        bool IsConnectionOpen();

        SqlConnection GetConnection { get; }//
    }
}